<?php
namespace Bosshartong\BotoLocations\Controller;

/***
 *
 * This file is part of the "Boto Locations" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

use Bosshartong\BotoLocations\Utility\ObjectUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * LocationController
 */
class LocationController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{


    /**
     * locationRepository
     *
     * @var \Bosshartong\BotoLocations\Domain\Repository\LocationRepository
     * @inject
     */
    protected $locationRepository = NULL;


    /**
     * @var \Bosshartong\BotoLocations\Domain\Repository\CategoryRepository
     * @inject
     */
    protected $categoryRepository;

    /**
     * @var \Bosshartong\BotoLocations\Domain\Repository\CountryRepository
     * @inject
     */
    protected $countryRepository;


    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {

        $contentObject = ObjectUtility::getConfigurationManager()->getContentObject()->data;
        $this->view->assign('contentObject', $contentObject);

        $selectedUids = \TYPO3\CMS\Core\Utility\GeneralUtility::intExplode(',', $this->settings['selectedLocations']);

        // 8.7 has an problem so we're forced to use dirty logic:
        // https://stackoverflow.com/questions/46471525/typo3-8-7-query-sorting-with-flexform-uid%C2%B4s
        //$locations = $this->locationRepository->findByUids($selectedUids);
        foreach($selectedUids as $key => $id){
            $locations[$id] = $this->locationRepository->findByUid($id);
        }


        $this->view->assign('locations', $locations);

    }

    /**
     * action showAll
     *
     * @return void
     */
    public function showAllAction()
    {

        $contentObject = ObjectUtility::getConfigurationManager()->getContentObject()->data;
        $this->view->assign('contentObject', $contentObject);

        $locations = $this->locationRepository->findAll();
        $this->view->assign('locations', $locations);
    }


    /**
     * action show
     *
     * @param \Bosshartong\BotoLocations\Domain\Model\Location $location
     * @return void
     */
    public function showAction(\Bosshartong\BotoLocations\Domain\Model\Location $location)
    {
        $contentObject = ObjectUtility::getConfigurationManager()->getContentObject()->data;
        $this->view->assign('contentObject', $contentObject);

        $this->view->assign('location', $location);
    }

    /**
     * action showMap
     *
     * @return void
     */
    public function showMapAction()
    {
        /*
        $GLOBALS['TSFE']->additionalHeaderData['botolocations'] = '
			<script src="https://maps.googleapis.com/maps/api/js?libraries=geometry,places&key=' . $this->settings['map']['apiKey'] . '"></script>
		';
        */

        $locations = $this->locationRepository->findAll();

        // get fixed map markers:
        $mapMarkersOnly = $this->locationRepository->findByType('1');

        // only if filtering is desired:
        if ((bool)$this->settings['map']['filter']) {
            // get the categories which are actually referenced. this will build the filters:
            $filterableCategories = $this->categoryRepository->findAllWithLocations();
            $this->view->assign('filterableCategories', $filterableCategories);

            $attributeCategories = $this->categoryRepository->findAllWithLocations('attribute1');
            $this->view->assign('filterableAttributes', $attributeCategories);

        }
        // only if filter or grouping are enabled:
        if ((bool)$this->settings['map']['filter'] || (bool)$this->settings['map']['groupListByCountry']) {
            // do we need special sorting for the countries - defined in TYPOSCRIPT
            if($this->settings['overrideCountrySorting']) {
                $manualSorting = explode(",", $this->settings['overrideCountrySorting']);
                foreach ($manualSorting as &$value) {
                    $filterCountries[] = $this->countryRepository->findByUid( $value);
                }
            } else {
                $filterCountries = $this->countryRepository->findAllWithCountries( 'cn_short_en', 'ASC');
            }
            $this->view->assign('filterableCountries', $filterCountries);
        }
        // only if grouping is enabled:
        if ((bool)$this->settings['map']['groupListByCountry']) {
            foreach($filterCountries as $filteredCountry) {
                $groupedLocationsByCountry[$filteredCountry->getUid()]['country'] = $filteredCountry;
                $groupedLocationsByCountry[$filteredCountry->getUid()]['locations'] = $this->locationRepository->findByCountry($filteredCountry->getUid());
            }
            $this->view->assign('groupedLocationsByCountry', $groupedLocationsByCountry);
        }

        $contentObject = ObjectUtility::getConfigurationManager()->getContentObject()->data;
        $config = $this->buildConfiguration();

        // this works ok:
        // get all locations for a specific category-uid:
        /*
        $collection = \TYPO3\CMS\Frontend\Category\Collection\CategoryCollection::load(
            68,
            TRUE,
            'tx_botolocations_domain_model_location',
            'categories'
        );
        $this->view->assign('collection', $collection);


        $testCategories = $this->categoryRepository->findByUidsRecursive([66,44]);
        $this->view->assign('test', $testCategories);
        */


        $this->view->assignMultiple([
            'locations' => $locations,
            'mapMarkersOnly' => $mapMarkersOnly,
            'contentObject' => $contentObject,
            'ids' => $config['ids'],
            'configuration' => $config['configuration']
        ]);

    }


    /**
     * Helper to build configuration.
     *
     * @return array
     */
    protected function buildConfiguration()
    {
        $contentObject = ObjectUtility::getConfigurationManager()->getContentObject()->data;

        $mapContainer ='tx-botolocations-' . $contentObject['uid'] . '-map';
        // Id's that will be used in the frontend.
        if($this->settings['map']['usealternativecontainer'] && $this->settings['map']['alternativecontainerid'] !='') {
            $mapContainer = trim(strtolower($this->settings['map']['alternativecontainerid']));
        }

        $ids = [
            'map' => $mapContainer,
            'searchRadius' => 'tx-botolocations-' . $contentObject['uid'] . '-search-radius',
            'listContainer' => 'tx-botolocations-' . $contentObject['uid'] . '-list',
            'countrySelector' => 'tx-botolocations-' . $contentObject['uid'] . '-countries',
            'attributeSelector' => 'tx-botolocations-' . $contentObject['uid'] . '-attributes',
            'mapMarkersOnly' => 'tx-botolocations-' . $contentObject['uid'] . '-mapmarkersonly',
            'fixed' => 'tx-botolocations-' . $contentObject['uid'] . '-fixed',
        ];

        // Configuration of the javascript library. This is defined in PHP, because it is much cleaner
        // than using f:format.raw in the frontend.
        $botolocationsConfiguration = [
            'apiKey' => $this->settings['map']['apiKey'],
            'mapContainer' => $ids['map'],
            'mapOptions' => [
                'mapTypeId' => $this->settings['map']['mapTypeId'],
                'zoom' => ((int)$this->settings['map']['zoom']) ? (int)$this->settings['map']['zoom'] : $this->settings['map']['defaultZoom'],
                'zoomControl' => (bool)$this->settings['map']['zoomControl'],
                'streetViewControl' => false,
                'defaultLat' => $this->settings['map']['defaultLat'],
                'defaultLong' => $this->settings['map']['defaultLong'],
                'defaultMarkerIconPath' => $this->settings['map']['defaultMarkerIconPath'],
                'center' => [
                    'lat' => 0,  // these will be falsy and thus the map will be automatically aligned
                    'lng' => 0
                ]
            ],
            'listContainer' => '#' . $ids['listContainer'],
            'countrySelector' => '#' . $ids['countrySelector'],
            'attributeSelector' => '#' . $ids['attributeSelector'],
            'mapMarkersOnly' => '#' . $ids['mapMarkersOnly'],
            'debug' => (bool)$this->settings['debug'],
            'controllerFactory' => [
                'showOnClick' => [],
                'infoWindow' => []
            ]
        ];

        if (!empty($this->settings['map']['style'])) {
            $botolocationsConfiguration['mapOptions']['styles'] = json_decode($this->settings['map']['style']);
        }

        if ((bool)$this->settings['search']['enable']) {
            $botolocationsConfiguration['controllerFactory']['autocompletedSearch'] = [
                'field' => '#' . $ids['searchField'],
                'expand' => (int) $this->settings['search']['expand']
            ];
        }

        if ((bool)$this->settings['map']['center']['latitude'] && (bool)$this->settings['map']['center']['longitude']) {
            $botolocationsConfiguration['mapOptions']['center'] = [
                'lat' => (float)$this->settings['map']['center']['latitude'],
                'lng' => (float)$this->settings['map']['center']['longitude'],
            ];
        }

        if ($this->settings['filter']['enable']) {
            $botolocationsConfiguration['controllerFactory']['tagFilter'] = [
                'items' => '#' . $ids['tags'] . ' > *:not(#' . $ids['resetTag'] . ')',
                'reset' => '#' . $ids['resetTag'],
                'container' => '#' . $ids['tagContainer'],
                'combine' => $this->settings['filter']['combine']
            ];
        }

        if ($this->settings['map']['enableLocking']) {
            $botolocationsConfiguration['controllerFactory']['clickToEnable'] = [
                'enabled' => '#' . $ids['lockEnabled'],
                'disabled' => '#' . $ids['lockDisabled']
            ];
        }

        if ($this->settings['map']['hideOnMobile']) {
            $botolocationsConfiguration['controllerFactory']['hideMapOnMobile'] = [];
        }

        return [
            'configuration' => $botolocationsConfiguration,
            'ids' => $ids
        ];
    }

}
