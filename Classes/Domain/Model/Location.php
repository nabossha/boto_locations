<?php
namespace Bosshartong\BotoLocations\Domain\Model;

/***
 *
 * This file is part of the "Boto Locations" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Location
 */
class Location extends AbstractEntity
{
    /**
     * title
     *
     * @var string
     * @validate NotEmpty
     */
    protected $title = '';

    /**
     * company
     *
     * @var string
     */
    protected $company = '';

    /**
     * address
     *
     * @var string
     */
    protected $address = '';

    /**
     * zip
     *
     * @var string
     */
    protected $zip = 0;

    /**
     * city
     *
     * @var string
     */
    protected $city = '';

    /**
     * country
     *
     * @var \Bosshartong\BotoLocations\Domain\Model\Country
     */
    protected $country;

    /**
     * phoneprimary
     *
     * @var string
     */
    protected $phoneprimary = '';

    /**
     * phonemobile
     *
     * @var string
     */
    protected $phonemobile = '';

    /**
     * fax
     *
     * @var string
     */
    protected $fax = '';

    /**
     * description
     *
     * @var string
     */
    protected $description = '';

    /**
     * email
     *
     * @var string
     */
    protected $email = '';

    /**
     * www
     *
     * @var string
     */
    protected $www = '';

    /**
     * latitude
     *
     * @var float
     */
    protected $latitude = 0.0;

    /**
     * geolong
     *
     * @var float
     */
    protected $longitude = 0.0;

    /**
     * image
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected $image = null;

    /**
     * detailpid
     *
     * @var string
     */
    protected $detailpid = '';

    /**
     * twitter
     *
     * @var string
     */
    protected $twitter = '';

    /**
     * facebook
     *
     * @var string
     */
    protected $facebook = '';

    /**
     * youtube
     *
     * @var string
     */
    protected $youtube = '';

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Bosshartong\BotoLocations\Domain\Model\Category>
     */
    protected $attribute1;


    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Bosshartong\BotoLocations\Domain\Model\Category>
     */
    protected $categories;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var bool
     */
    protected $isheadquarter;

    /**
     * @var bool
     */
    protected $isnotfilterable;


    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->categories = new ObjectStorage();
    }


    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the phoneprimary
     *
     * @return string $phoneprimary
     */
    public function getPhoneprimary()
    {
        return $this->phoneprimary;
    }

    /**
     * Sets the phoneprimary
     *
     * @param string $phoneprimary
     * @return void
     */
    public function setPhoneprimary($phoneprimary)
    {
        $this->phoneprimary = $phoneprimary;
    }

    /**
     * Returns the phonemobile
     *
     * @return string $phonemobile
     */
    public function getPhonemobile()
    {
        return $this->phonemobile;
    }

    /**
     * Sets the phonemobile
     *
     * @param string $phonemobile
     * @return void
     */
    public function setPhonemobile($phonemobile)
    {
        $this->phonemobile = $phonemobile;
    }

    /**
     * Returns the fax
     *
     * @return string $fax
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Sets the fax
     *
     * @param string $fax
     * @return void
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    /**
     * Returns the email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets the email
     *
     * @param string $email
     * @return void
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Returns the description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description
     *
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Returns the latitude
     *
     * @return float $latitude
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Sets the latitude
     *
     * @param float $latitude
     * @return void
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * Returns the longitude
     *
     * @return float $longitude
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Sets the longitude
     *
     * @param float $longitude
     * @return void
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * Returns the image
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Sets the image
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     * @return void
     */
    public function setImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image)
    {
        $this->image = $image;
    }


    /**
     * Returns the detailpid
     *
     * @return string $detailpid
     */
    public function getDetailpid()
    {
        return $this->detailpid;
    }

    /**
     * Sets the detailpid
     *
     * @param string $detailpid
     * @return void
     */
    public function setDetailpid($detailpid)
    {
        $this->detailpid = $detailpid;
    }

    /**
     * Returns the country
     *
     * @return \SJBR\StaticInfoTables\Domain\Model\Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Sets the country
     *
     * @param \SJBR\StaticInfoTables\Domain\Model\Country $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * Returns the address
     *
     * @return string address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Sets the address
     *
     * @param string $address
     * @return void
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * Returns the company
     *
     * @return string $company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Sets the company
     *
     * @param string $company
     * @return void
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * Returns the www
     *
     * @return string $www
     */
    public function getWww()
    {
        return $this->www;
    }

    /**
     * Sets the www
     *
     * @param string $www
     * @return void
     */
    public function setWww($www)
    {
        $this->www = $www;
    }

    /**
     * Returns the zip
     *
     * @return string $zip
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Sets the zip
     *
     * @param string $zip
     * @return void
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * Returns the city
     *
     * @return string $city
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Sets the city
     *
     * @param string $city
     * @return void
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * Returns the twitter
     *
     * @return string $twitter
     */
    public function getTwitter()
    {
        return $this->twitter;
    }

    /**
     * Sets the twitter
     *
     * @param string $twitter
     * @return void
     */
    public function setTwitter($twitter)
    {
        $this->twitter = $twitter;
    }

    /**
     * Returns the facebook
     *
     * @return string $facebook
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * Sets the facebook
     *
     * @param string $facebook
     * @return void
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;
    }

    /**
     * Returns the youtube
     *
     * @return string $youtube
     */
    public function getYoutube()
    {
        return $this->youtube;
    }

    /**
     * Sets the youtube
     *
     * @param string $youtube
     * @return void
     */
    public function setYoutube($youtube)
    {
        $this->youtube = $youtube;
    }

    /**
     * Adds a Locationattributes
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\Category $attribute1
     * @return void
     */
    public function addAttribute1($attribute1)
    {
        $this->attribute1->attach($attribute1);
    }

    /**
     * Removes a Locationattributes
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\Category $attribute1ToRemove The Category to be removed
     * @return void
     */
    public function removeAttribute1($attribute1ToRemove)
    {
        $this->attribute1->detach($attribute1ToRemove);
    }

    /**
     * Returns the attribute1
     *
     * @return ObjectStorage
     */
    public function getAttribute1()
    {
        return $this->attribute1;
    }

    /**
     * Sets the attribute1
     *
     * @param ObjectStorage $attribute1
     */
    public function setAttribute1(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $attribute1)
    {
        $this->attribute1 = $attribute1;
    }

    /**
     * Adds a Category
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\Category $category
     * @return void
     */
    public function addCategory(\TYPO3\CMS\Extbase\Domain\Model\Category $category)
    {
        $this->categories->attach($category);
    }
    /**
     * Removes a Category
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\Category $category The Category to be removed
     * @return void
     */
    public function removeCategory(\TYPO3\CMS\Extbase\Domain\Model\Category $category)
    {
        $this->categories->detach($category);
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $categories
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;
    }


    /**
     * Returns a comma separated list containing the uids of the caterories for the filter
     * The categories are filterCategories, if the enableFilterCategories setting is enabled
     * and normal categories, if not.
     *
     * @return string
     */
    public function getCategoryUids()
    {
        $uids = [];
        foreach ($this->getCategories() as $category) {
            $uids[] = $category->getUid();
        }
        return implode(',', $uids);
    }

    /**
     * Returns a comma separated list containing the uids of the caterories for the filter
     * The categories are filterCategories, if the enableFilterCategories setting is enabled
     * and normal categories, if not.
     *
     * @return string
     */
    public function getAttribute1Uids()
    {
        $uids = [];
        foreach ($this->getAttribute1() as $category) {
            $uids[] = $category->getUid();
        }
        return implode(',', $uids);
    }

    /**
     * Get type of location
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type of location
     *
     * @param int $type type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Get isheadquarter flag
     *
     * @return bool
     */
    public function getIsheadquarter()
    {
        return $this->isheadquarter;
    }

    /**
     * Set isheadquarter flag
     *
     * @param bool $isheadquarter flag
     */
    public function setIsheadquarter($isheadquarter)
    {
        $this->isheadquarter = $isheadquarter;
    }

    /**
     * Get isnotfilterable flag
     *
     * @return bool
     */
    public function getIsnotfilterable()
    {
        return $this->isnotfilterable;
    }

    /**
     * Set isnotfilterable flag
     *
     * @param bool $isnotfilterable flag
     */
    public function setIsnotfilterable($isnotfilterable)
    {
        $this->isnotfilterable = $isnotfilterable;
    }

}
