<?php
namespace Bosshartong\BotoLocations\Domain\Repository;

/***************************************************************
 *
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;

/**
 * The repository for Locations
 */
class LocationRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * @var \Bosshartong\BotoLocations\Domain\Repository\CategoryRepository
     * @inject
     */
    protected $categoryRepository;

    /**
     * @var \Bosshartong\BotoLocations\Domain\Repository\CountryRepository
     * @inject
     */
    protected $countryRepository;


	// Order by:
	protected $defaultOrderings = array(
		'company' => QueryInterface::ORDER_ASCENDING,
        'title' => QueryInterface::ORDER_ASCENDING,
        'uid'   => QueryInterface::ORDER_DESCENDING
	);


    /**
     * Initialize Object with predefined settings
     *
     * @return void
     */
    public function initializeObject() {

        /** @var Typo3QuerySettings $querySettings */
        $querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
        $querySettings->setRespectStoragePage(FALSE);
        $this->setDefaultQuerySettings($querySettings);
    }


    /**
     * Finds locations by their uids
     *
     * @param int[] $uids
     * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByUids(array $uids)
    {
        $query = $this->createQuery();
        $query->matching($query->in('uid', $uids));
        return $query->execute();
    }


    public function findByCategory(Category $category)
    {
        $query = $this->createQuery();
        $query->matching($query->contains('categories', $category));
        return $query->execute();
    }

    /**
     * Finds locations by their uids
     *
     * @param int[] $uids
     * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByCountry($country)
    {
        $query = $this->createQuery();
        $query->matching($query->equals('country', $country));
        return $query->execute();
    }




    /**
     * Finds locations grouped by country
     *
     * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findAllGroupedByCountry()
    {
        $query = $this->createQuery();
        //$countries = $this->countryRepository->findAllWithCountries();
        //\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump( $countries, '$result in testSql' );
        //die();
        $query->statement('
            SELECT DISTINCT
                static_countries.*
            FROM
                static_countries
            JOIN
                tx_botolocations_domain_model_location
                ON tx_botolocations_domain_model_location.country = static_countries.uid
            WHERE
                 tx_botolocations_domain_model_location.country = static_countries.uid
                 '.$this->enableFields("tx_botolocations_domain_model_location") .'
                 
            GROUP BY
                static_countries.uid
            ORDER BY 
              static_countries.uid
        ');
        //$result = $query->execute();
       // \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump( $result, '$result in testSql' );
       // die();
        return $query->execute();
    }

    /**
     * Returns an enableFields SQL statement for the specified table
     * @param  string $tableName  name of the database table
     * @return string             enableFields SQL statement
     */
    protected function enableFields($tableName) {
        if (TYPO3_MODE === 'FE') {
            // Use enableFields in frontend mode
            $enableFields = $GLOBALS['TSFE']->sys_page->enableFields($tableName);
        } else {
            // Use enableFields in backend mode
            $enableFields = \TYPO3\CMS\Backend\Utility\BackendUtility::deleteClause($tableName);
            $enableFields .= \TYPO3\CMS\Backend\Utility\BackendUtility::BEenableFields($tableName);
        }

        return $enableFields;
    }


}