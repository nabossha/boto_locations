<?php
namespace Bosshartong\BotoLocations\Domain\Repository;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Extbase\Persistence\QueryInterface;

/**
 * Country repository.
 */
class CountryRepository extends \SJBR\StaticInfoTables\Domain\Repository\CountryRepository
{

    /**
     * Find record by given uid, returning it raw.
     *
     * @param int $uid
     * @return array
     */
    public function findRawByUid($uid)
    {
        $query = $this->createQuery();
        $query->matching(
            $query->equals('uid', $uid)
        );
        $query->setLimit(1);
        return $query->execute(true)[0];
    }

    /**
     * Find all countries used in all locations.
     *
     * @return ObjectStorage
     */
    public function findAllWithCountries($orderfield = 'cn_iso_2', $sorting = 'ASC')
    {
        $query = $this->createQuery();
        $query->statement('
            SELECT DISTINCT
                static_countries.*
            FROM
                static_countries
            JOIN
                tx_botolocations_domain_model_location
                ON tx_botolocations_domain_model_location.country = static_countries.uid
            WHERE
                 tx_botolocations_domain_model_location.country = static_countries.uid
                 AND  tx_botolocations_domain_model_location.type = 0
                '.$this->enableFields("tx_botolocations_domain_model_location") .'
            GROUP BY
                static_countries.uid
            ORDER BY 
              static_countries.'.$orderfield.' '.$sorting.'
        ');
        return $query->execute();
    }


    /**
     * Returns an enableFields SQL statement for the specified table
     * @param  string $tableName  name of the database table
     * @return string             enableFields SQL statement
     */
    protected function enableFields($tableName) {
        if (TYPO3_MODE === 'FE') {
            // Use enableFields in frontend mode
            $enableFields = $GLOBALS['TSFE']->sys_page->enableFields($tableName);
        } else {
            // Use enableFields in backend mode
            $enableFields = \TYPO3\CMS\Backend\Utility\BackendUtility::deleteClause($tableName);
            $enableFields .= \TYPO3\CMS\Backend\Utility\BackendUtility::BEenableFields($tableName);
        }

        return $enableFields;
    }


}
