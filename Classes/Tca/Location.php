<?php
namespace Bosshartong\BotoLocations\Tca;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2011 Xavier Perseguers <xavier@causal.ch>, Causal
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 * Google map.
 *
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 */
class Location
{

    /**
     * Renders the Google map.
     *
     * @param array $PA
     * @param \TYPO3\CMS\Backend\Form\FormEngine $pObj
     * @return string
     */
    public function renderMap(array &$PA, \TYPO3\CMS\Backend\Form\Element\UserElement $pObj)
    {
        $out = array();
        $latitude = (float)$PA['row'][$PA['parameters']['latitude']];
        $longitude = (float)$PA['row'][$PA['parameters']['longitude']];

        $baseElementId = isset($PA['itemFormElID']) ? $PA['itemFormElID'] : $PA['table'] . '_map';
        $addressId = $baseElementId . '_address';
        $mapId = $baseElementId . '_map';

        if (!($latitude && $longitude)) {
            $latitude = 47.475647;
            $longitude = 9.477882;
        };

        $dataPrefix = 'data[' . $PA['table'] . '][' . $PA['row']['uid'] . ']';
        $latitudeField = $dataPrefix . '[' . $PA['parameters']['latitude'] . ']';
        $longitudeField = $dataPrefix . '[' . $PA['parameters']['longitude'] . ']';

        $out[] = '<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?language=en"></script>';
        $out[] = '<script type="text/javascript">';
        $out[] = <<<EOT
if (typeof GeoCoder == 'undefined') GeoCoder = {};

GeoCoder.init = function() {
	GeoCoder.origin = new google.maps.LatLng({$latitude}, {$longitude});
	var myOptions = {
		zoom: 13,
		center: GeoCoder.origin,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	GeoCoder.map = new google.maps.Map(document.getElementById("{$mapId}"), myOptions);
	GeoCoder.marker = new google.maps.Marker({
		map: GeoCoder.map,
		position: GeoCoder.origin,
		draggable: true
	});
	google.maps.event.addListener(GeoCoder.marker, 'dragend', function() {

        GeoCoder.updateCoordinates();

		// Update address
		GeoCoder.reverseGeocode(GeoCoder.marker.getPosition().lat(), GeoCoder.marker.getPosition().lng());

	});
	GeoCoder.geocoder = new google.maps.Geocoder();
	GeoCoder.reverseGeocode({$latitude}, {$longitude});
};

GeoCoder.updateCoordinates = function() {
    var lat = GeoCoder.marker.getPosition().lat().toFixed(6);
    var lng = GeoCoder.marker.getPosition().lng().toFixed(6);

    // Update visible + hidden fields
    TYPO3.jQuery("input[data-formengine-input-name*='{$latitudeField}']").val(lat);
    TYPO3.jQuery("input[name='{$latitudeField}']").val(lat);
    
    TYPO3.jQuery("input[data-formengine-input-name*='{$longitudeField}']").val(lng);
    TYPO3.jQuery("input[name='{$longitudeField}']").val(lng);

}

GeoCoder.refreshMap = function() {
	google.maps.event.trigger(GeoCoder.map, 'resize');
}

GeoCoder.codeAddress = function() {
	var address = document.getElementById("{$addressId}").value;
	GeoCoder.geocoder.geocode({'address': address}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			GeoCoder.map.setCenter(results[0].geometry.location);
			GeoCoder.marker.setPosition(results[0].geometry.location);

			GeoCoder.updateCoordinates();
		} else {
			alert("Geocode was not successful for the following reason: " + status);
		}
	});
}

GeoCoder.reverseGeocode = function(latitude, longitude) {
	var latlng = new google.maps.LatLng(latitude, longitude);
	GeoCoder.geocoder.geocode({'latLng': latlng}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK && results[1]) {
			var address = document.getElementById("{$addressId}");
			address.value = results[1].formatted_address;
		}
	});
}

window.onload = GeoCoder.init;
EOT;
        $out[] = '</script>';
        $out[] = '<div id="' . $baseElementId . '">';
        $out[] = '
			<input id="' . $addressId . '" type="textbox" value="" style="width:360px">
			<input type="button" value="Show" onclick="GeoCoder.codeAddress()">
			<p style="margin:1em 0">drag the marker to update the GPS coordinates above</p>
		';
        $out[] = '<div id="' . $mapId . '" style="height:340px;width:600px"></div>';
        $out[] = '</div>'; // id=$baseElementId

        return implode('', $out);
    }

}
