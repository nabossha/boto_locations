<?php
defined('TYPO3_MODE') or die();

call_user_func(function () {

    $customLanguageFilePrefix = 'LLL:EXT:boto_locations/Resources/Private/Language/locallang_db.xlf:';
    $frontendLanguageFilePrefix = 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:';

    // Add the CType "boto_slider"
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
        'tt_content',
        'CType',
        [
            'LLL:EXT:boto_locations/Resources/Private/Language/locallang_db.xlf:wizard.title',
            'botolocations_location',
            'content-image'
        ],
        'textmedia',
        'after'
    );

    // set the icon-identifier:
    $GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['botolocations_location'] = 'content-botolocations';

    // Define what fields to display
    $GLOBALS['TCA']['tt_content']['types']['botolocations_location'] = [
        'showitem' => 'l10n_parent, l10n_diffsource,
                --palette--;' . $frontendLanguageFilePrefix . 'palette.general;general,
                header,
                 --linebreak--,
                header_layout;' . $frontendLanguageFilePrefix . 'header_layout_formlabel,
                --linebreak--,
                pi_flexform,
                --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
                    --palette--;;language,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
                    --palette--;;hidden,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
				--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.extended,
        ',
    ];

});
