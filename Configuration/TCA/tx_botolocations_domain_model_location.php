<?php
defined('TYPO3_MODE') or die();

$frontendLanguageFilePrefix = 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:';

$tx_botolocations_domain_model_location = [
    'ctrl' => [
        'title' => 'LLL:EXT:boto_locations/Resources/Private/Language/locallang_db.xlf:tx_botolocations_domain_model_location',
        'label' => 'company',
        'label_alt' => 'title',
        'label_alt_force' => 1,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'hideAtCopy' => true,
        //'sortby' => 'sorting',
        'default_sortby' => 'ORDER BY company ASC',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'type' => 'type',
        'typeicon_column' => 'type',
        'typeicon_classes' => [
            'default' => 'ext-botolocations-type-default',
            '1' => 'ext-botolocations-type-mapmarker',
        ],
        'useColumnsForDefaultValues' => 'type',
        'searchFields' => 'title,company,address',
        'iconfile' => 'EXT:boto_locations/Resources/Public/Icons/tx_botolocations_domain_model_location.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, company, address, zip, city, country, phoneprimary, phonemobile, fax, description, email, www, type, isheadquarter,isnotfilterable,geolat, geolong, image, detailpid, twitter, facebook, youtube, attribute1, categories',
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_botolocations_domain_model_location',
                'foreign_table_where' => 'AND tx_botolocations_domain_model_location.pid=###CURRENT_PID### AND tx_botolocations_domain_model_location.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.hidden'
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
            ],
        ],
        'title' => [
            'exclude' => true,
            'label' => 'LLL:EXT:boto_locations/Resources/Private/Language/locallang_db.xlf:tx_botolocations_domain_model_location.title',
            'l10n_mode' => 'prefixLangTitle',
            'config' => [
                'type' => 'text',
                'cols' => 30,
                'rows' => 2,
                'eval' => 'trim,required'
            ],
        ],
        'company' => [
            'exclude' => true,
            'l10n_mode' => 'exclude',
            'label' => 'LLL:EXT:boto_locations/Resources/Private/Language/locallang_db.xlf:tx_botolocations_domain_model_location.company',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'address' => [
            'exclude' => true,
            'l10n_mode' => 'exclude',
            'label' => 'LLL:EXT:boto_locations/Resources/Private/Language/locallang_db.xlf:tx_botolocations_domain_model_location.address',
            'config' => [
                'type' => 'text',
                'cols' => 30,
                'rows' => 4,
                'eval' => 'trim'
            ]
        ],
        'zip' => [
            'exclude' => true,
            'l10n_mode' => 'exclude',
            'label' => 'LLL:EXT:boto_locations/Resources/Private/Language/locallang_db.xlf:tx_botolocations_domain_model_location.zip',
            'config' => [
                'type' => 'input',
                'size' => 8,
                'eval' => 'trim'
            ]
        ],
        'city' => [
            'exclude' => true,
            'l10n_mode' => 'exclude',
            'label' => 'LLL:EXT:boto_locations/Resources/Private/Language/locallang_db.xlf:tx_botolocations_domain_model_location.city',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'country' => [
            'exclude' => true,
            'l10n_mode' => 'exclude',
            'label' => 'LLL:EXT:boto_locations/Resources/Private/Language/locallang_db.xlf:tx_botolocations_domain_model_location.country',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'static_countries',
                'foreign_table_where' => 'ORDER BY cn_short_en ASC',
                'size' => 1,
                'minitems' => 1,
                'maxitems' => 1,
                'wizards' => [
                    'suggest' => [
                        'type' => 'suggest',
                    ],
                ],
            ],
        ],
        'phoneprimary' => [
            'exclude' => true,
            'l10n_mode' => 'exclude',
            'label' => 'LLL:EXT:boto_locations/Resources/Private/Language/locallang_db.xlf:tx_botolocations_domain_model_location.phoneprimary',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'phonemobile' => [
            'exclude' => true,
            'l10n_mode' => 'exclude',
            'label' => 'LLL:EXT:boto_locations/Resources/Private/Language/locallang_db.xlf:tx_botolocations_domain_model_location.phonemobile',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'fax' => [
            'exclude' => true,
            'l10n_mode' => 'exclude',
            'label' => 'LLL:EXT:boto_locations/Resources/Private/Language/locallang_db.xlf:tx_botolocations_domain_model_location.fax',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'description' => [
            'exclude' => true,
            'label' => 'LLL:EXT:boto_locations/Resources/Private/Language/locallang_db.xlf:tx_botolocations_domain_model_location.description',
            'config' => [
                'type' => 'text',
                'cols' => 30,
                'rows' => 5,
                'eval' => 'trim'
            ]
        ],
        'www' => [
            'exclude' => true,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:boto_locations/Resources/Private/Language/locallang_db.xlf:tx_botolocations_domain_model_location.www',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'softref' => 'typolink,url',
                'wizards' => [
                    '_PADDING' => 2,
                    'link' => [
                        'type' => 'popup',
                        'title' => 'LLL:EXT:cms/locallang_ttc.xml:header_link_formlabel',
                        'icon' => 'link_popup.gif',
                        'module' => [
                            'name' => 'wizard_element_browser',
                            'urlParameters' => [
                                'mode' => 'wizard',
                                'act' => 'url|page'
                            ]
                        ],
                        'params' => [
                            'blindLinkOptions' => 'mail,file,spec,folder',
                        ],
                        'JSopenParams' => 'height=300,width=500,status=0,menubar=0,scrollbars=1',
                    ],
                ],
            ],
        ],
        'email' => [
            'exclude' => true,
            'l10n_mode' => 'exclude',
            'label' => 'LLL:EXT:boto_locations/Resources/Private/Language/locallang_db.xlf:tx_botolocations_domain_model_location.email',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'max' => 255,
                'softref' => 'email'
            ],
        ],
        'type' => [
            'exclude' => false,
            'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.doktype_formlabel',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['LLL:EXT:boto_locations/Resources/Private/Language/locallang_db.xlf:tx_botolocations_domain_model_location.type.I.0', 0, 'ext-botolocations-type-default'],
                    ['LLL:EXT:boto_locations/Resources/Private/Language/locallang_db.xlf:tx_botolocations_domain_model_location.type.I.1', 1, 'ext-botolocations-type-mapmarker'],
                ],
                'showIconTable' => true,
                'size' => 1,
                'maxitems' => 1,
            ]
        ],
        'isheadquarter' => [
            'exclude' => true,
            'l10n_mode' => 'exclude',
            'label' => 'LLL:EXT:boto_locations/Resources/Private/Language/locallang_db.xlf:tx_botolocations_domain_model_location.isheadquarter',
            'config' => [
                'type' => 'check',
                'default' => 0
            ]
        ],
        'isnotfilterable' => [
            'exclude' => true,
            'l10n_mode' => 'exclude',
            'label' => 'LLL:EXT:boto_locations/Resources/Private/Language/locallang_db.xlf:tx_botolocations_domain_model_location.isnotfilterable',
            'config' => [
                'type' => 'check',
                'default' => 0
            ]
        ],
        'geolocalization' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:boto_locations/Resources/Private/Language/locallang_db.xlf:tx_botolocations_domain_model_location.geolocalization',
            'config' => [
                'type' => 'user',
                'userFunc' => 'Bosshartong\\BotoLocations\\Tca\\Location->renderMap',
                'parameters' => [
                    'latitude' => 'latitude',
                    'longitude' => 'longitude',
                ],
            ],
        ],
        'latitude' => [
            'exclude' => true,
            'l10n_mode' => 'exclude',
            'label' => 'LLL:EXT:boto_locations/Resources/Private/Language/locallang_db.xlf:tx_botolocations_domain_model_location.geolat',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'double6'
            ],
        ],
        'longitude' => [
            'exclude' => true,
            'l10n_mode' => 'exclude',
            'label' => 'LLL:EXT:boto_locations/Resources/Private/Language/locallang_db.xlf:tx_botolocations_domain_model_location.geolong',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'double6'
            ],
        ],
        'image' => [
            'exclude' => true,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:boto_locations/Resources/Private/Language/locallang_db.xlf:tx_botolocations_domain_model_location.image',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'image',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference'
                    ],
                    'foreign_types' => [
                        '0' => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ]
                    ],
                    'maxitems' => 5,
                    'minitems' => 0,
                ],
                $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
            ),
        ],
        'detailpid' => [
            'exclude' => true,
            'l10n_mode' => 'exclude',
            'label' => 'LLL:EXT:boto_locations/Resources/Private/Language/locallang_db.xlf:tx_botolocations_domain_model_location.detailpid',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'softref' => 'typolink,url',
                'wizards' => [
                    '_PADDING' => 2,
                    'link' => [
                        'type' => 'popup',
                        'title' => 'LLL:EXT:cms/locallang_ttc.xml:header_link_formlabel',
                        'icon' => 'link_popup.gif',
                        'module' => [
                            'name' => 'wizard_element_browser',
                            'urlParameters' => [
                                'mode' => 'wizard',
                                'act' => '|page'
                            ]
                        ],
                        'params' => [
                            'blindLinkOptions' => 'url,mail,file,spec,folder',
                        ],
                        'JSopenParams' => 'height=300,width=500,status=0,menubar=0,scrollbars=1',
                    ],
                ],
            ],
        ],
        'twitter' => [
            'exclude' => true,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:boto_locations/Resources/Private/Language/locallang_db.xlf:tx_botolocations_domain_model_location.twitter',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'facebook' => [
            'exclude' => true,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:boto_locations/Resources/Private/Language/locallang_db.xlf:tx_botolocations_domain_model_location.facebook',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'youtube' => [
            'exclude' => true,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:boto_locations/Resources/Private/Language/locallang_db.xlf:tx_botolocations_domain_model_location.youtube',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'attribute1' => [
            'exclude' => true,
            'l10n_mode' => 'exclude',
            'label' => 'LLL:EXT:boto_locations/Resources/Private/Language/locallang_db.xlf:tx_botolocations_domain_model_location.attribute1',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectTree',
                'MM' => 'sys_category_record_mm',
                'MM_match_fields' => [
                    'fieldname' => 'attribute1',
                    'tablenames' => 'tx_botolocations_domain_model_location',
                ],
                'MM_opposite_field' => 'items',
                'foreign_table' => 'sys_category',
                'foreign_table_where' => ' AND (sys_category.sys_language_uid = 0 OR sys_category.l10n_parent = 0) ORDER BY sys_category.sorting',
                'minitems' => 0,
                'maxitems' => 99,
                'appearance' => [
                    'collapseAll' => 0,
                    'size' => 10,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1
                ],
            ],
        ],
        'categories' => [
            'exclude' => true,
            'l10n_mode' => 'exclude',
            'label' => 'LLL:EXT:boto_locations/Resources/Private/Language/locallang_db.xlf:tx_botolocations_domain_model_location.categories',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectTree',
                'MM' => 'sys_category_record_mm',
                'MM_match_fields' => [
                    'fieldname' => 'categories',
                    'tablenames' => 'tx_botolocations_domain_model_location',
                ],
                'MM_opposite_field' => 'items',
                'foreign_table' => 'sys_category',
                'foreign_table_where' => ' AND (sys_category.sys_language_uid = 0 OR sys_category.l10n_parent = 0) ORDER BY sys_category.sorting',
                'minitems' => 0,
                'maxitems' => 99,
                'appearance' => [
                    'collapseAll' => 0,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1
                ],
            ],
        ],
    ],
    'types' => [
        // default location
        '0' => ['showitem' =>
                    'l10n_parent, l10n_diffsource,
                --palette--;;paletteBasic,
                --palette--;;paletteType,
                --palette--;;paletteCoreData,
                --div--;LLL:EXT:boto_locations/Resources/Private/Language/locallang_db.xlf:div.geoinfo,
                        latitude,longitude,geolocalization, 
                --div--;LLL:EXT:boto_locations/Resources/Private/Language/locallang_db.xlf:div.socialmedia,
                    --palette--;;paletteSocialmedia, 

                --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access,
                        starttime, endtime'
        ],
        // map marker only location
        '1' => ['showitem' =>
                    'l10n_parent, l10n_diffsource,
                --palette--;;paletteBasic,
                --palette--;;paletteType,
                --palette--;' . $frontendLanguageFilePrefix . 'palette.general;paletteCoreData,
                --div--;LLL:EXT:boto_locations/Resources/Private/Language/locallang_db.xlf:div.geoinfo,
                        latitude,longitude,geolocalization, 
                --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access,
                        starttime, endtime'
        ],
    ],

    'palettes' => [
        'paletteBasic' => [
            'showitem' => 'hidden;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:field.default.hidden, --linebreak--,sys_language_uid,--linebreak--,',
        ],
        'paletteType' => [
            'showitem' => '
                type,--linebreak--,
                isheadquarter, isnotfilterable
                ',
            'canNotCollapse' => 1
        ],
        'paletteCoreData' => [
            'showitem' => ', 
                            title, --linebreak--,
							company, --linebreak--,
							address, --linebreak--,
							zip, --linebreak--,
							city, --linebreak--,
							country, --linebreak--,
							phoneprimary, --linebreak--,
							phonemobile, --linebreak--,						
							fax, --linebreak--,
                            email, --linebreak--,	
							description, --linebreak--,
							www, --linebreak--,
                            detailpid, --linebreak--, 
                            image',
            'canNotCollapse' => 1
        ],
        'paletteSocialmedia' => [
            'showitem' => ', 
                             twitter, --linebreak--,facebook, --linebreak--,youtube, ',
            'canNotCollapse' => 1
        ],
        'attributes' => [
            'showitem' => 'attribute1, --linebreak--,
                categories',
            'canNotCollapse' => 1
        ],
    ],
];

return $tx_botolocations_domain_model_location;