<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Bosshartong.' . $_EXTKEY,
    'Location',
    array(
        'TtContent' => 'show',
        'Location' => 'show,list',
    ),
    // non-cacheable actions
    array(
        'Location' => '',
        'TtContent' => '',
    ),
    // as content element
    'CType'
);

// register individual type icons
if (TYPO3_MODE === 'BE') {
    $icons        = [
        'content-botolocations' => 'content-botolocations.svg',
        'ext-botolocations-type-default' => 'content-botolocations-location.svg',
        'ext-botolocations-type-mapmarker' => 'content-botolocations-mapmarker.svg',
    ];
    $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
    foreach ($icons as $identifier => $path) {
        $iconRegistry->registerIcon(
            $identifier,
            \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
            ['source' => 'EXT:boto_locations/Resources/Public/Icons/' . $path]
        );
    }
}


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig ('

mod.wizards.newContentElement.wizardItems.common.elements.botolocations_location {
    iconIdentifier = content-botolocations
    title = LLL:EXT:boto_locations/Resources/Private/Language/locallang_db.xlf:boto_locations_CE_title
    description = LLL:EXT:boto_locations/Resources/Private/Language/locallang_db.xlf:boto_locations_CE_description
    tt_content_defValues {
        CType = botolocations_location
    }
}
mod.wizards.newContentElement.wizardItems.common.show := addToList(botolocations_location)

');

// Register EXT-Icon
$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
    \TYPO3\CMS\Core\Imaging\IconRegistry::class
);
$iconRegistry->registerIcon(
    'pages-contains-botolocations', // Icon-Identifier
    \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
    ['source' => 'EXT:boto_locations/Resources/Public/Icons/folder-icon.png']
);



//if (TYPO3_MODE == 'BE') {
// Rendering hook of content elements in backend
/*
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem'][] =
  'EXT:boto_locations/Classes/Hooks/DrawItem.php:JUBU\Botocontacts\Hooks\DrawItem';
//}
*/