/**
 * init.js
 *
 */
BotoLocationMap = {};
BotoLocationMap.Bootstrap = $.extend({}, {
    bootstrappers: [],
    bootstrap: function () {
        BotoLocationMap.Bootstrap._invokeBootstrappers();
    },
    registerBootstrap: function (bootstrap) {
        BotoLocationMap.Bootstrap.bootstrappers.push(bootstrap);
    },
    _invokeBootstrappers: function () {
        $.each(BotoLocationMap.Bootstrap.bootstrappers, function (index, bootstrapper) {
            bootstrapper.initialize();
        });
    }
});

$(function () {
    BotoLocationMap.Bootstrap.bootstrap();
});


BotoLocationMap.MapController = $.extend({}, {
    Configuration: [],
    BotoLocationData: [],
    markers: [],
    originalBounds: [],
    CountryFilterArray: [],
    Attribute1FilterArray: [],

    initialize: function () {
        // not used here
    },
    boot: function(config) {
        this.Configuration = config;
        console.log(this.Configuration);

        this.getLocationData();
        this.loadMap();
    },
    loadMap: function() {
        logBuch("loading Google Maps API...");
        jQuery.getScript("https://maps.googleapis.com/maps/api/js" +
            "?callback=BotoLocationMap.MapController.mapLoaded" +
            "&key="+this.Configuration['apiKey']+
            "&libraries=places" +
            "&sensor=true&language="+jQuery('body').data("language")
        );
    },
    getLocationData: function() {
        jQuery('.botolocation-item').each(function() {
            var $thisItem = jQuery(this);
            var tempLocation = [];
            tempLocation['title'] = $thisItem.find('.item-title').text();
            tempLocation['content'] = $thisItem.html();
            tempLocation['properties'] = [];
            jQuery.each(jQuery(this).data(), function(i, v) {
                tempLocation['properties'][i] = v;
            });
            BotoLocationMap.MapController.BotoLocationData.push(tempLocation);
        });
        console.log(BotoLocationMap.MapController.BotoLocationData);
    },

    filterMapbyCountry: function(country) {
        jQuery.ajax({
            type: 'GET',
            dataType: 'json',
            url: 'https://maps.googleapis.com/maps/api/geocode/json?components=country:'+country+'&key=' +this.Configuration['apiKey'],
            success: function (data) {
                if (typeof data['results'][0]['geometry'] != 'undefined') {
                    var bounds = {
                        north: data.results[0].geometry.viewport.northeast.lat,
                        south: data.results[0].geometry.viewport.southwest.lat,
                        east: data.results[0].geometry.viewport.northeast.lng,
                        west: data.results[0].geometry.viewport.southwest.lng
                    };
                    LocationMap.setZoom(16);
                    LocationMap.fitBounds(bounds);       //auto-zoom
                }
            }
        });
    },
    mapLoaded: function() {
        logBuch("Google Maps loaded");
        LocationMap = new google.maps.Map(document.getElementById(this.Configuration['mapContainer']),{
            center: this.Configuration['mapOptions']['center'],
            zoom: this.Configuration['mapOptions']['zoom'],
            zoomControl: this.Configuration['mapOptions']['zoomControl'],
            mapTypeControl: false,
            mapTypeId: this.Configuration['mapOptions']['mapTypeId']
        });
        LocationGeocoder = new google.maps.Geocoder();

        if (this.Configuration['mapOptions']['styles']) {
            var styledMap = new google.maps.StyledMapType(
                this.Configuration['mapOptions']['styles'],
                {name: "customized-map"}
            );
            LocationMap.mapTypes.set('map_style', styledMap);
            LocationMap.setMapTypeId('map_style');
        }

        // bounds for auto-zoom/auto-center:
        var bounds = new google.maps.LatLngBounds();

        //  multiple markers on a map
        infoWindow = new google.maps.InfoWindow();
        for (var i = 0; i < Object.keys(BotoLocationMap.MapController.BotoLocationData).length; i++) {
            var location = BotoLocationMap.MapController.BotoLocationData[i];
            var latLng = new google.maps.LatLng(location['properties'].locationLatitude,location['properties'].locationLongitude);
            var markerIcon = this.Configuration['mapOptions']['defaultMarkerIcon'];
            if(location.contact === '1') {
                markerIcon = this.defaults.markerIconContact;
            }
            //console.log(location['properties'].categoryUids);

            bounds.extend(latLng);
            var marker = new google.maps.Marker({
                position: latLng,
                map: LocationMap,
                title:location.title,
                content:location.title,
                uid: location.uid,
                icon: markerIcon,
                properties: {

                }
            });
            // info window:
            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    infoWindow.setContent(this.content);
                    infoWindow.open(LocationMap, marker);
                    //BotoLocationMap.MapController.getCountryfromLocation(this.uid);
                }
            })(marker, i));
            this.markers.push(marker);
        }

        // Add a marker clusterer to manage the markers.
        var markerCluster = new MarkerClusterer(
            LocationMap,
            this.markers,
            {   gridSize: 10,
                maxZoom: 15,
                imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
            }
            );

        BotoLocationMap.MapController.originalBounds = bounds;
        LocationMap.fitBounds(bounds);       //auto-zoom
        LocationMap.panToBounds(bounds);     // auto-center
        this.addListeners();
    },
    addListeners: function() {
        jQuery(this.Configuration['countrySelector']).change(function(e) {
            BotoLocationMap.MapController.filterByAttribute('location-country-iso',this.value);
            // if country-filter, apply new map-layout:
            BotoLocationMap.MapController.mapCountry(this.value);
        });
        jQuery(this.Configuration['attributeSelector']).change(function(e) {
            BotoLocationMap.MapController.filterByAttribute('attribute1-uids',this.value);
        });

        // build the global available filters:

        jQuery(this.Configuration['countrySelector'] + ' > option').each(function() {
            BotoLocationMap.MapController.CountryFilterArray.push(jQuery(this).val());
        });
        jQuery(this.Configuration['attributeSelector'] + ' > option').each(function() {
            BotoLocationMap.MapController.Attribute1FilterArray.push(jQuery(this).val());
        });
        console.log(this.Attribute1FilterArray);
    },
    // attribute-filtering:
    filterByAttribute: function(attribute, uid) {
        var containerSelector = jQuery(this.Configuration['listContainer']).find('.botolocation-item');
        if(uid == '*') {
            containerSelector.show();
        } else {
            containerSelector.hide().filter('*[data-'+attribute+'*="'+uid+'"]').show();
        }
        // respect grouping
        jQuery(this.Configuration['listContainer'] + ' > .botolocations-countrygroup').each(function() {
            jQuery(this).show();
            var groupSize = jQuery(this).find('ul li:visible').length;
            if(groupSize === 0) {
                jQuery(this).hide();
            }
        });
        this.checkFilterDependencies();
    },

    checkFilterDependencies: function() {
        jQuery(this.Configuration['listContainer'] + ' ul li:visible').each(function() {
          // console.log("countries:"+jQuery(this).data('location-country-uid'));
          //  console.log("attribute1:"+jQuery(this).data('attribute1-uids'));
        });
        // disable not available options:


    },

    filterMarkers: function (markerArr) {
        for (i = 0; i < markers1.length; i++) {
            marker = gmarkers1[i];
            // If is same category or category not picked
            if (marker.category == category || category.length === 0) {
                marker.setVisible(true);
            }
            // Categories don't match
            else {
                marker.setVisible(false);
            }
        }
        LocationMap.fitBounds(bounds);       //auto-zoom
        LocationMap.panToBounds(bounds);     // auto-center
    },
    mapCountry: function(country) {
        LocationGeocoder.geocode({
                "country": country,
                componentRestrictions: {
                    country: country
                }
        }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                    LocationMap.setZoom(16);
                    LocationMap.fitBounds(results[0].geometry.viewport);
            } else {
                // use default values
                LocationMap.fitBounds( BotoLocationMap.MapController.originalBounds);
            }
        });
    },
    reLayoutMap: function() {
        try {
            infoWindow.close();
        } catch(e) {
            //
        }
        // do we have filters?

        // apply them

        // reposition map:
        //LocationMap.setCenter(ALXMap.MapController.defaults.userPosition);
        //LocationMap.fitBounds(originalBounds);
    }
});

if (typeof BotoLocationMap.Bootstrap !== 'undefined') {
    BotoLocationMap.Bootstrap.registerBootstrap(BotoLocationMap.MapController);
}

// obj.length function for older borwsers:
if (!Object.keys) {
    Object.keys = function (obj) {
        var keys = [],
            k;
        for (k in obj) {
            if (Object.prototype.hasOwnProperty.call(obj, k)) {
                keys.push(k);
            }
        }
        return keys;
    };
}
