<?php
defined('TYPO3_MODE') || die('Access denied.');

/*
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    $_EXTKEY,
    'Location',
    'Locationstandort'
);
*/

$extensionName = strtolower(\TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($_EXTKEY));
$pluginSignature = $extensionName.'_location';
// botolocations_location

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';

// Add a flexform to the CType
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    '',
    'FILE:EXT:boto_locations/Configuration/FlexForms/Flexform_LocationCE.xml',
    $pluginSignature
);

////////////////////////////////////////////////////////////////////////////
//
// Add pagetree icons
$TCA['pages']['columns']['module']['config']['items'][] = array('BoTo Locations', 'botolocat', 'pages-contains-botolocations');
$GLOBALS['TCA']['pages']['ctrl']['typeicon_classes']['contains-botolocat'] = 'pages-contains-botolocations';


call_user_func(
    function()
    {

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('boto_locations', 'Configuration/TypoScript', 'BoTo Locations');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_botolocations_domain_model_location', 'EXT:boto_locations/Resources/Private/Language/locallang_csh_tx_botolocations_domain_model_location.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_botolocations_domain_model_location');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
            'boto_locations',
            'tx_botolocations_domain_model_location',
            'attribute1', // field for categories
            array(
                // Set a custom label
                'label' => 'LLL:EXT:boto_locations/Resources/Private/Language/locallang_db.xlf:tx_botolocations_domain_model_location.attribute1',
                // Override generic configuration, e.g. sort by title rather than by sorting
                'fieldConfiguration' => array(
                    'foreign_table_where' => ' AND sys_category.sys_language_uid IN (-1, 0) ORDER BY sys_category.title ASC',
                ),
                // string (keyword), see TCA reference for details
                //'l10n_mode' => 'exclude',
                // list of keywords, see TCA reference for details
                //'l10n_display' => 'hideDiff',
            )
        );


        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
            'boto_locations',
            'tx_botolocations_domain_model_location',
            'categories', // field for categories
            array(
                // Set a custom label
                'label' => 'LLL:EXT:boto_locations/Resources/Private/Language/locallang_db.xlf:tx_botolocations_domain_model_location.categories',
                // Override generic configuration, e.g. sort by title rather than by sorting
                'fieldConfiguration' => array(
                    'foreign_table_where' => ' AND sys_category.sys_language_uid IN (-1, 0) ORDER BY sys_category.title ASC',
                ),
                // string (keyword), see TCA reference for details
                //'l10n_mode' => 'exclude',
                // list of keywords, see TCA reference for details
                //'l10n_display' => 'hideDiff',
            )
        );

    }
);
